import numpy as np
from typing import Sequence, Generator, Tuple, List
from mlp.functions import softmax_function, sigmoid_function
from mlp.layers import layer_perceptron



 # Initialize the Multi-Layer Perceptron (MLP) with specified layers and loss function
class MLP:
    def __init__(self, layers: Sequence[int], loss_function: callable):
        self.layers = list(self.__generating_layers(layers))
        self.loss_function = loss_function

    # Forward pass through the network to make predictions
    def predict(self, inputs: np.ndarray):
        values = inputs
        for layer in self.layers:
            values = layer.forward(values)
        return values


    # Calculate the loss between predictions and targets using the specified loss function
    def loss(self, predictions: np.ndarray, targets: np.ndarray):
        return self.loss_function(predictions, targets)

    def trainig(self,
                data: List[Tuple[np.ndarray, np.ndarray]],
                learning_rate: float = 0.05,
                n_epochs: int = 1500) -> Generator[Tuple[int, float, float], None, None]:
        for epoch in range(n_epochs):
            sum_loss = 0.0
            num_trained_inputs = 0

            for inputs, targets in data:
                num_trained_inputs += inputs.shape[0]
                preds = self.predict(inputs)
                sum_loss += self.loss(preds, targets).sum()
                self._backward(preds, targets)

                # Calculate accuracy for the entire epoch
                for layer in self.layers:
                    layer.weights_matrix -= learning_rate * layer.grad_w
                    layer.biases -= learning_rate * layer.grad_b
                accuracy = sum([
                    p.argmax() == t.argmax()
                    for p, t in zip(preds, targets)
                ]) / len(targets)
            yield epoch, sum_loss / num_trained_inputs, accuracy

    def _backward(self, predictions: np.ndarray, targets: np.ndarray):
        grad = self.loss_function.backward(predictions, targets)
        for i in range(len(self.layers)-1, -1, -1):
            grad = self.layers[i].backward(grad)

    @staticmethod
    def __generating_layers(layers: Sequence[int]) -> Generator[layer_perceptron, None, None]:
        previous_layer_size = layers[0]
        for i, layer_size in enumerate(layers[1:]):
            is_last_layer = (i == len(layers) - 2)
            yield layer_perceptron(
                input_size=previous_layer_size,
                n_units=layer_size,
                activation_function=softmax_function() if is_last_layer else sigmoid_function()
            )
            previous_layer_size = layer_size
