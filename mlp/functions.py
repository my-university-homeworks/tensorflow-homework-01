import numpy as np

# activation functions


class sigmoid_function:
    def __call__(self, inputs: np.ndarray) -> np.ndarray:
        self.old_y = np.array([
            1 / (1 + np.exp(-x))
            for x in inputs
        ])
        return self.old_y

    def backward(self, grad: np.ndarray):
        return np.array([
            y * (1. - y) * g
            for y, g in zip(self.old_y, grad)
        ])

class softmax_function:
    def __call__(self, inputs: np.ndarray) -> np.ndarray:
        self.old_y = np.array([
            np.exp(z) / np.sum(np.exp(z))
            for z in inputs
        ])
        return self.old_y

    def backward(self, grad: np.ndarray):
        return np.array([
            y * (grad - (grad * y).sum(axis=1)[:, np.newaxis])
            for y in self.old_y
        ])



# loss functions



class cross_entropy_loss:
    def __call__(self, predictions: np.ndarray, targets: np.ndarray) -> np.ndarray:
        return np.array([
            -np.sum(ts * np.log(ps + 1e-100))
            for ps, ts in zip(predictions, targets)
        ])

    def backward(self, predictions: np.ndarray, targets: np.ndarray) -> np.ndarray:
        return np.array([
            (ps - ts) / len(ts)
            for ps, ts in zip(predictions, targets)
        ])
