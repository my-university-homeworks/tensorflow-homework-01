from typing import Callable

import numpy as np

class layer_perceptron:
    def __init__(self,
                 activation_function: Callable[[np.ndarray], np.ndarray],
                 n_units: int,
                 input_size: int):
        self.activation_function = activation_function
        self.nunits = n_units
        self.input_size = input_size
        self.weights_matrix = np.random.normal(0, 0.2, (input_size, n_units))
        self.biases = np.zeros(n_units)

    def forward(self, inputs: np.ndarray) -> np.ndarray:
        self.old_inputs = inputs
        return self.activation_function(
            np.dot(inputs, self.weights_matrix) + self.biases
        )

    def backward(self, grad: np.ndarray):
        self.grad_b = grad.mean(axis=0)
        self.grad_w = (np.matmul(self.old_inputs[:, :, np.newaxis], grad[:, np.newaxis, :])).mean(axis=0)
        return np.dot(grad, self.weights_matrix.transpose())
