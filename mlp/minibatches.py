import random
from typing import Iterable, Tuple, Generator

import numpy as np
import random
from typing import Iterable, Tuple, Generator

import numpy as np
def generate_random_minibatches(data: Iterable[Tuple[np.ndarray, np.ndarray]], minibatch_size: int = 100) -> Generator[Tuple[np.ndarray, np.ndarray], None, None]:
    shuffled = list(data)
    random.shuffle(shuffled)

    for i in range(0, len(shuffled), minibatch_size):
        yield (
            np.array([image for image, _ in shuffled[i:i + minibatch_size]]),
            np.array([t for _, t in shuffled[i:i + minibatch_size]])
        )
