
import sklearn.datasets
from matplotlib import pyplot as plt

import random
from typing import Iterable, Tuple, Generator
import numpy as np

from mlp.functions import cross_entropy_loss
from mlp.mlp import MLP
from mlp.minibatches import generate_random_minibatches

# Load the dataset
dataset = sklearn.datasets.load_digits()

# Prepare the data
datatarget_pairs = list(zip(dataset.data.astype(np.float32) / 16.0, np.eye(10)[dataset.target]))

# Split data and targets into shuffled minibatches
minibatches = list(generate_random_minibatches(datatarget_pairs, minibatch_size=50))

# Create and train the MLP
mlp = MLP(layers=[64, 20, 10], loss_function=cross_entropy_loss())
n_epochs = 5_000
loss_recording = np.zeros(n_epochs)
accuracy_recording = np.zeros(n_epochs)

# generate random minibatches
def generating_random_minibatches(data: Iterable[Tuple[np.ndarray, np.ndarray]], minibatch_size: int = 100) -> Generator[Tuple[np.ndarray, np.ndarray], None, None]:
    shuffled = list(data)
    random.shuffle(shuffled)

    for i in range(0, len(shuffled), minibatch_size):
        yield (
            np.array([image for image, _ in shuffled[i:i + minibatch_size]]),
            np.array([t for _, t in shuffled[i:i + minibatch_size]])
        )

# Training the mlp
mlp_training = mlp.trainig(minibatches, learning_rate=0.03, n_epochs=n_epochs)

for epoch, loss, accuracy in mlp_training:
    if (epoch + 1) % 25 == 0 or epoch in (0, n_epochs - 1):
        print(f"Epoch {epoch+1}/{n_epochs} ({round(epoch*100.0/n_epochs, 1)}%): "
              f"loss = {round(loss, 5)}, accuracy = {round(accuracy*100, 2)}%")
    loss_recording[epoch] = loss
    accuracy_recording[epoch] = accuracy

# Plot loss over epochs
fig, ax = plt.subplots()
plt.title("Loss and Accuracy over Training Epochs")
plt.xlabel("Training Epoch")

ax.plot(loss_recording, color='red', label='Avg. Cross Entropy Loss')
ax.tick_params(axis='y', labelcolor='red')
ax.set_ylabel('Avg. Cross Entropy Loss', color='red')

ax2 = ax.twinx()
ax2.plot(accuracy_recording * 100, color='green', label='Accuracy (%)')
ax2.tick_params(axis='y', labelcolor='green')
ax2.set_ylabel('Accuracy (%)', color='green')

plt.show()



